{ inputs, ... }: {
  imports = with inputs.self.nixosProfiles; [
    boot
    locale
    misc
    network
    user

    packages
    plasma
    xdg
    audio
    firefox
    thunderbird
    mimeapps
    zsh
    fonts
  ];
}
