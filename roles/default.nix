{
  base = ./base.nix;
  code = ./code.nix;
  pro = ./pro.nix;
  perso = ./perso.nix;
  gaming = ./gaming.nix;
}
