{ pkgs, inputs, ... }: {
    imports = with inputs.self.nixosProfiles; [
        ./code.nix

        adb
        gcloud
        kubernetes
    ];

    home-manager.users.romain.home.packages = with pkgs; [
        terraform
        zoom-us
        slack
        openvpn
        gnupg
        pinentry
    ];
}