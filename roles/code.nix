{ pkgs, inputs, ...}: {
    imports = with inputs.self.nixosProfiles; [
        docker
        adb
    ];

    home-manager.users.romain.home.packages = with pkgs; [
        cherrytree
        gnome-pomodoro
        (texlive.combine { inherit (texlive) scheme-medium xifthen ifmtarg framed paralist titlesec clearsans textpos; })
        texstudio

        insomnia
        #postman
        emacs
        asdf-vm

        #android-studio
        #jetbrains.datagrip
        jetbrains.ruby-mine
        #jetbrains.webstorm

        jdk17
        nodejs_18
        yarn
        watchman
        nodePackages_latest.gitmoji-cli
        nodePackages_latest.pnpm

        exercism
        elixir
    ];
}
