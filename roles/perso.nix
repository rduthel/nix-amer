{ lib, pkgs, inputs, ... }: {
  imports = with inputs.self.nixosProfiles; [
    ./base.nix
    adb
  ];

  home-manager.users.romain.home.packages = with pkgs; [
    nextcloud-client
    clementine
    freetube
    kdenlive
    yt-dlp
    mullvad-vpn
    musescore
    rustdesk
    kdePackages.kasts
  ];

  # Disable touchpad support (enabled default in most desktopManager).
  # broken with plasma6
  # services.libinput.enable = lib.mkDefault false;

  services.openssh.enable = true;
  services.mullvad-vpn.enable = true;
}
