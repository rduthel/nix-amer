inputs: final: prev: {
  yt-dlp = prev.yt-dlp.overrideAttrs {
    src = final.fetchPypi {
      version = "2024.11.18";
      pname = "yt_dlp";
      hash = "sha256-uKTCPTya/X5Ha824fzi2wOjhLjojnXmI8TrLQ0IA9U0=";
    };
  };
}
