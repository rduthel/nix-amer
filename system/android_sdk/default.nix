with import <nixos-unstable> {
  config = {
    android_sdk.accept_license = true;
    allowUnfree = true;
  };
};

let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };

  buildToolsVersion = "34.0.0";
  cmakeVersion = "3.22.1";

  androidComposition = androidenv.composeAndroidPackages {
    cmdLineToolsVersion = "12.0-rc15";
    buildToolsVersions = [ buildToolsVersion "30.0.3" "33.0.0" ];
    platformVersions = [ "30" "33" "34" ];
    abiVersions = [ "x86_64"];
    cmakeVersions = [cmakeVersion];
    includeEmulator = true;
    includeNDK = true;
    ndkVersions = ["26.1.10909125"];
    toolsVersion = "26.1.1";

    includeSources = true;
    includeSystemImages = true;
    systemImageTypes = [ "google_apis" "google_apis_playstore" ];
    useGoogleAPIs = true;
    useGoogleTVAddOns = true;
    includeExtras = [
      "extras;google;gcm"
    ];
  };
in mkShell rec {
  buildInputs = [
   jdk17
   unstable.nodejs_18
   unstable.android-studio
   unstable.jetbrains.idea-ultimate
  ];

  ANDROID_SDK_ROOT = "${androidComposition.androidsdk}/libexec/android-sdk";
  ANDROID_NDK_ROOT = "${androidComposition.androidsdk}/libexec/android-sdk/ndk";
  ANDROID_HOME = "${ANDROID_SDK_ROOT}";
  AVDMANAGER_EXECUTABLE = "${ANDROID_SDK_ROOT}/avdmanager";
  EMULATOR_EXECUTABLE = "${ANDROID_SDK_ROOT}/emulator";
  CHROME_EXECUTABLE = "${google-chrome}/bin/google-chrome-stable";
  GRADLE_OPTS = "-Dorg.gradle.project.android.aapt2FromMavenOverride=${ANDROID_SDK_ROOT}/build-tools/${buildToolsVersion}/aapt2";

  # Use the same cmakeVersion here
  shellHook = ''
      # Add cmake to the path.
      cmake_root="$(echo "$ANDROID_SDK_ROOT/cmake/${cmakeVersion}"*/)"
      export PATH="$cmake_root/bin:$PATH"
      export PATH="$EMULATOR_EXECUTABLE:$PATH"
      export PATH="$ANDROID_SDK_ROOT/platform-tools:$PATH"
      export PATH="$ANDROID_SDK_ROOT/tools:$PATH"
    '';
}
