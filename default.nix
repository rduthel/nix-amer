{ ... }:
let
  self = (import (let lock = builtins.fromJSON (builtins.readFile ./flake.lock);
  in fetchTarball {
    url =
      "https://github.com/9999years/flake-compat/archive/${lock.nodes.flake-compat_2.locked.rev}.tar.gz";
    sha256 = lock.nodes.flake-compat_2.locked.narHash;
  }) { src = ./.; }).defaultNix;
in self // self.legacyPackages.x86_64-linux
