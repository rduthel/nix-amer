{ pkgs, lib, inputs, config, ... }: {
  nix = rec {
    nixPath = lib.mkForce [ "self=/etc/self/compat" "nixpkgs=/etc/nixpkgs" ];
    registry.self.flake = inputs.self;
    registry.np.flake = inputs.nixpkgs;

    nrBuildUsers = config.nix.settings.max-jobs;

    optimise.automatic = true;

    settings = {
      use-xdg-base-directories = true;
      builders-use-substitutes = true;
      flake-registry = "${inputs.flake-registry}/flake-registry.json";
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  environment.etc.nixpkgs.source = inputs.nixpkgs;
  environment.etc.self.source = inputs.self;
}
