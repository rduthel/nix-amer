{ config, ... }:

let
    lock = builtins.fromJSON (builtins.readFile ../flake.lock);

    localRanges = [{
        from = 1714;
        to = 1764;
        }]; # KDE connect
in {
  networking = {
    networkmanager = {
      enable = true;

      # Disable NetworkManager's internal DNS resolution
      dns = "none";
    };
    nameservers = [ "4.2.2.6" ];

    # These options are unnecessary when managing DNS ourselves
    useDHCP = false;
    dhcpcd.enable = false;

    extraHosts =
      ''
        0.0.0.0 sdk.privacy-center.org
        ${let
          locks = lock.nodes.steven-black-lists.locked;
          hostsFile = builtins.fetchTarball {
            url = "https://${locks.type}.com/${locks.owner}/${locks.repo}/archive/${locks.rev}.tar.gz";
            sha256 = locks.narHash;
          };

        in builtins.readFile "${hostsFile}/alternates/fakenews-gambling/hosts"}
      '';
    firewall = {
      enable = true;
      interfaces.wlan0.allowedTCPPortRanges = localRanges;
      interfaces.wlan0.allowedUDPPortRanges = localRanges;
      interfaces.eth0.allowedUDPPortRanges = localRanges;
      interfaces.eth0.allowedTCPPortRanges = localRanges;
      trustedInterfaces = [ "eth0" ];
    };

    hostName = config.device;
  };
}
