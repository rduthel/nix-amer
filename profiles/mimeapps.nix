{ ... }:

{
  home-manager.users.romain.xdg.mimeApps = {
    enable = true;
    associations.added = {
      "application/x-yaml" = ["org.kde.kate.desktop" "emacs.desktop" "okularApplication_txt.desktop"];
      "x-scheme-handler/http" = "firefox.desktop";
      "x-scheme-handler/https" = "firefox.desktop";
      "inode/directory" = "org.kde.dolphin.desktop";
      "x-scheme-handler/mailto" = "thunderbird.desktop.desktop";
      "x-scheme-handler/mid" = "thunderbird.desktop.desktop";
      "x-scheme-handler/geo" = "openstreetmap-geo-handler.desktop";
      "x-scheme-handler/tel" = "org.kde.kdeconnect.handler.desktop";
      "audio/mp4" = ["vlc.desktop" "mpv.desktop" "org.clementine_player.Clementine.desktop"];
      "audio/mpeg" = ["vlc.desktop" "mpv.desktop" "org.clementine_player.Clementine.desktop"];
      "audio/ogg" = ["vlc.desktop" "mpv.desktop" "org.clementine_player.Clementine.desktop"];
      "image/svg+xml" = ["org.inkscape.Inkscape.desktop" "gimp.desktop" "firefox.desktop" "org.kde.kate.desktop"];
      "text/markdown" = ["org.kde.kate.desktop" "okularApplication_md.desktop" "emacs.desktop"];
      "text/plain" = ["org.kde.kate.desktop" "emacs.desktop" "okularApplication_txt.desktop"];
      "video/mp4" = ["vlc.desktop" "mpv.desktop"];
      "video/mpeg" = ["vlc.desktop" "mpv.desktop"];
      "video/ogg" = ["vlc.desktop" "mpv.desktop"];
      "video/webm" = ["vlc.desktop" "mpv.desktop"];
    };

    associations.removed = {
      "application/x-yaml" = ["writer.desktop" "emacsclient.desktop" "org.kde.kwrite.desktop"];
      "audio/mp4" = "umpv.desktop";
      "audio/mpeg" = "umpv.desktop";
      "audio/ogg" = "umpv.desktop";
      "image/svg+xml" = ["writer.desktop" "okularApplication_txt.desktop" "emacsclient.desktop" "org.kde.kwrite.desktop"];
      "text/markdown" = ["writer.desktop" "okularApplication_txt.desktop" "emacsclient.desktop" "org.kde.kwrite.desktop"];
      "text/plain" = ["writer.desktop" "org.kde.kwrite.desktop" "emacsclient.desktop"];
      "video/mp4" = "umpv.desktop";
      "video/mpeg" = "umpv.desktop";
      "video/ogg" = ["umpv.desktop" "org.clementine_player.Clementine.desktop"];
      "video/webm" = "umpv.desktop";
    };

    defaultApplications = {
      "application/x-yaml" = "org.kde.kate.desktop";
      "audio/mp4" = "vlc.desktop";
      "audio/mpeg" = "vlc.desktop";
      "audio/ogg" = "vlc.desktop";
      "image/svg+xml" = "org.inkscape.Inkscape.desktop";
      "message/rfc822" = "thunderbird.desktop.desktop";
      "text/markdown" = "org.kde.kate.desktop";
      "text/plain" = "org.kde.kate.desktop";
      "video/mp4" = "vlc.desktop";
      "video/mpeg" = "vlc.desktop";
      "video/ogg" = "vlc.desktop";
      "video/webm" = "vlc.desktop";

      "x-scheme-handler/http" = "firefox.desktop";
      "x-scheme-handler/https" = "firefox.desktop";
      "inode/directory" = "org.kde.dolphin.desktop";
      "x-scheme-handler/mailto" = "thunderbird.desktop.desktop";
      "x-scheme-handler/mid" = "thunderbird.desktop.desktop";
      "x-scheme-handler/geo" = "openstreetmap-geo-handler.desktop";
      "x-scheme-handler/tel" = "org.kde.kdeconnect.handler.desktop";

      "x-scheme-handler/sgnl" = "signal-desktop.desktop";
      "x-scheme-handler/signalcaptcha" = "signal-desktop.desktop";
    };
  };
}
