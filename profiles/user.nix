{ config, pkgs, lib, ... }: {

# Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.romain = {
    isNormalUser = true;
    description = "Romain Duthel";
    extraGroups = [ "networkmanager" "wheel" "input" "adbusers" "adm" ];
  };
}
