{ pkgs, lib, config, inputs, ... }: {

  home-manager.users.romain = {
    news.display = "silent";

    systemd.user.startServices = true;

    home.stateVersion = lib.mkDefault "22.11";
  };

  home-manager.useGlobalPkgs = true;
  home-manager.backupFileExtension = "backup_$(date -u +%s)";

  system.stateVersion = lib.mkDefault "22.11";
}
