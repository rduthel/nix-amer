{ config, pkgs, ...}:
let
  profileName = "Par défaut";
in {
  services = {
    xserver.enable = true;
    displayManager.sddm.enable = true;
    desktopManager.plasma6.enable = true;
  };

    programs.partition-manager.enable = true;
    programs.kdeconnect.enable = true;

    environment.plasma6.excludePackages = with pkgs.kdePackages; [ elisa ];

    home-manager.users.romain = {
      home.packages = with pkgs; [ tela-circle-icon-theme ];
      programs.konsole = {
        enable = true;
        defaultProfile = profileName;
        profiles = {
          options = {
            name = profileName;
            colorScheme = "BlackOnWhite";
            font.name = "MesloLGS Nerd Font";
          };
        };
      };
      programs.plasma = {
          enable = true;
          kwin = {
            effects = {
              dimAdminMode.enable = true;
              minimization.animation = "magiclamp";
              translucency.enable = true;
              wobblyWindows.enable = true;
            };
            virtualDesktops = {
              number = 4;
              rows = 2;
            };
            titlebarButtons = {
              left = [ "close" "maximize" "minimize" ];
              right = [ "on-all-desktops" "keep-above-windows" ];
            };
          };
          startup = {
            startupScript = {
              options = {
                text = "yakuake";
                priority = 8;
              };
            };
          };
          workspace = {
            cursor = { theme = "Breeze_Snow"; };
            iconTheme = "Tela-circle-light";
            wallpaper = "/home/romain/nix-amer/profiles/plasma/wallpaper.jpg";
          };
        };
    };
}
