{  config, pkgs, ... }: {
home-manager.users.romain.home.packages = with pkgs; [
    docker
    docker-compose
];

virtualisation.docker = {
    enable = true;
    rootless.enable = true;
    rootless.setSocketVariable = true;
    enableOnBoot = true;
  };
}
