{ config, pkgs, ... }: {
  fonts.packages = with pkgs; [
    nerd-fonts.aurulent-sans-mono
    nerd-fonts.code-new-roman
    nerd-fonts.cousine
    nerd-fonts.droid-sans-mono
    nerd-fonts.fira-code
    nerd-fonts.fira-mono
    nerd-fonts.jetbrains-mono
    nerd-fonts.lekton
    nerd-fonts.liberation
    nerd-fonts.meslo-lg
    nerd-fonts.monoid
    nerd-fonts.noto
    nerd-fonts.roboto-mono
    nerd-fonts.ubuntu
    nerd-fonts.ubuntu-mono
    nerd-fonts.ubuntu-sans
  ];
}
