{ pkgs, lib, config, ... }:

let
    gcloud = pkgs.google-cloud-sdk.withExtraComponents(
        with pkgs.google-cloud-sdk.components; [ gke-gcloud-auth-plugin ]
      );
in
{
    home-manager.users.romain = {
        home.packages = [ gcloud ];
    };
}
