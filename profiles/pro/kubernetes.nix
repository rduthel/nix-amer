{ config, pkgs, ... }:

let
    kubeMasterIP = "127.0.0.1"; #10.1.1.2
    kubeMasterHostname = "api.kube";
    kubeMasterAPIServerPort = 6443;
in {
    networking.extraHosts = ''
        ${kubeMasterIP} ${kubeMasterHostname}
    '';

#    environment.systemPackages = with pkgs; [
#        kompose
#        kubectl
#        kubernetes
#    ];

    home-manager.users.romain.home.packages = with pkgs; [
        k9s
        minikube
        kompose
        kubectl
        kubernetes
        openlens
    ];

  services.kubernetes = {
    roles = ["master" "node"];
    masterAddress = kubeMasterHostname;
    apiserverAddress = "https://${kubeMasterHostname}:${toString kubeMasterAPIServerPort}";
    easyCerts = true;
    apiserver = {
      securePort = kubeMasterAPIServerPort;
      advertiseAddress = kubeMasterIP;
    };

    # use coredns
    addons.dns.enable = true;
  };
}