{ pkgs, config, lib, inputs, ... }: {
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    nano
    wget
    git
  ];

  home-manager.users.romain.home.packages = with pkgs;
    [
        /* UTILITY */
        ark
        file
        filelight
        imagemagick
        kate
        keepassxc
        kronometer
        ktouch
        mat2
        partition-manager
        popsicle

        # ---
        kdePackages.kalarm

        /* DEV */
        meld

        /* GRAPHIC */
        gimp
        inkscape
        # ---
        kdePackages.kcolorpicker

        /* INTERNET */
        discord
        falkon
        links2
        remmina
        signal-desktop
        # ---
        kdePackages.plasma-browser-integration

        /* MULTIMEDIA */
        peek
        vlc
        # ---
        kdePackages.krecorder

        /* OFFICE */
        libreoffice
        pdfarranger

        /* SYSTEM */
        freshfetch
        htop

        kdePackages.yakuake
    ] ++ lib.optionals config.deviceSpecific.goodMachine [

    ];
}
