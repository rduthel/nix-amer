{ pkgs, ... }:

{
home-manager.users.romain.programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland;

    policies =
      {
        DontCheckDefaultBrowser = true;
        DisableTelemetry = true;
        DisableFirefoxStudies = true;
        DisablePocket = true;

        DisplayBookmarksToolbar = "never";
        DisplayMenuBar = "default-off";

        OverrideFirstRunPage = "";
        PictureInPicture.Enabled = true;
        PromptForDownloadLocation = false;

        HardwareAcceleration = true;
        TranslateEnabled = false;

        Homepage.StartPage = "homepage";

        DownloadDirectory = "\$\{home\}/Bureau";

        DisableMasterPasswordCreation = true;
        OfferToSaveLogins = false;

        HttpsOnlyMode = "enabled";

        RequestedLocales = ["fr" "en"];

        SanitizeOnShutdown = true;

        UserMessaging =
        {
          UrlbarInterventions = false;
          SkipOnboarding = true;
        };

        FirefoxSuggest =
        {
          WebSuggestions = false;
          SponsoredSuggestions = false;
          ImproveSuggest = false;
        };

        EnableTrackingProtection =
        {
          Value = true;
          Cryptomining = true;
          Fingerprinting = true;
          EmailTracking = true;
        };

        FirefoxHome =
        {
          Search = true;
          TopSites = false;
          SponsoredTopSites = false;
          Highlights = false;
          Pocket = false;
          SponsoredPocket = false;
          Snippets = false;
        };
      };

    profiles.default = {
      isDefault = true;
      search = {
        default = "DuckDuckGo";
        engines = {
          "Bing".metaData.hidden = true;
          "Amazon.fr".metaData.hidden = true;
          "Google".metaData.hidden = true;
          "Youtube".metaData.hidden = true;
        };
      };
      settings = {
        # Sécurité
        ## Do Not Track
        "privacy.donottrackheader.enabled" = true;


        # Vie privée
        ## Paramètres personnalisés pour l’historique
        "privacy.history.custom" = true;

        ## Ne pas afficher les suggestions de recherche en navigation privée
        "browser.search.separatePrivateDefault.urlbarResult.enabled" = false;

        ## Effacer l’historique récent
        "privacy.cpd.cache" = true;
        "privacy.cpd.cookies" = true;
        "privacy.cpd.sessions" = true;
        "privacy.cpd.offlineApps" = true;
        "privacy.cpd.siteSettings" = true;
        "privacy.cpd.history" = false;
        "privacy.cpd.formdata" = false;
        "privacy.cpd.downloads" = false;

        ## Onglets conteneurs
        "privacy.userContext.enabled" = true;
        "privacy.userContext.ui.enabled" = true;
        "privacy.userContext.longPressBehavior" = 2;
        "privacy.userContext.extension" = "CookieAutoDelete@kennydo.com";


        # Media
        ## Formulaires interactifs dans les PDF
        "app.normandy.startupRolloutPrefs.pdfjs.renderInteractiveForms" = true;

        ## Mode lecture
        "reader.font_size" = 7;
        "reader.content_width" = 4;
        "reader.font_type" = "serif";
        "reader.color_scheme" = "sepia";


        # UX / DX
        ## Avertir lors de la fermeture de plusieurs onglets
        "browser.tabs.warnOnClose" = true;
        # Ne pas avertir quand Ctrl+Q
        "browser.warnOnQuitShortcut" = false;

        ## Synchronisation
        "services.sync.engine.history" = false;
        "services.sync.engine.passwords" = false;
        "services.sync.engine.prefs.modified" = false;
        "services.sync.declinedEngines" = "history,forms,passwords,creditcards";

        ## Outils de développement
        "devtools.theme" = "light";
        "devtools.cache.disabled" = true;
        "devtools.chrome.enabled" = true;
        "devtools.application.enabled" = false;
        "devtools.accessibility.enabled" = false;
        "devtools.debugger.ignore-caught-exceptions" = false;
        "devtools.aboutdebugging.collapsibilities.processes" = false;


        # Recherche et formulaires
        ## Recherche
        "browser.urlbar.placeholderName" = "DuckDuckGo";
        "browser.urlbar.placeholderName.private" = "DuckDuckGo";
        "browser.urlbar.showSearchSuggestionsFirst" = false;
        "browser.urlbar.timesBeforeHidingSuggestionsHint" = 0;

        ## Remplissage auto
        "extensions.formautofill.creditCards.enabled" = false;
        "extensions.formautofill.addresses.usage.hasEntry" = false;


        # Interface
        "browser.uiCustomization.state" = builtins.toJSON
          {
            placements =
            {
              widget-overflow-fixed-list = [];
              toolbar-menubar = [ "menubar-items" ];
              PersonalToolbar = [ "personal-bookmarks" "managed-bookmarks" ];
              nav-bar =
              [
                "back-button"
                "forward-button"
                "stop-reload-button"
                "home-button"
                "urlbar-container"
                "_7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1_-browser-action"
                "wappalyzer_crunchlabz_com-browser-action"
                "downloads-button"
                "_72b2e02b-3a71-4895-886c-fd12ebe36ba3_-browser-action"
                "_testpilot-containers-browser-action"
                "_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action"
                "keepassxc-browser_keepassxc_org-browser-action"
                "https-everywhere-eff_eff_org-browser-action"
                "_f73df109-8fb4-453e-8373-f59e61ca4da3_-browser-action"
                "_e824f0e4-72f7-4295-8879-d8ff4bf348d2_-browser-action"
                "_8455ae24-8d2d-40dd-89a6-6ebca545dbb5_-browser-action"
                "fox_replace_fx-browser-action"
                "_b7f9d2cd-d772-4302-8c3f-eb941af36f76_-browser-action"
                "_a6c4a591-f1b2-4f03-b3ff-767e5bedf4e7_-browser-action"
                "reset-pbm-toolbar-button"
                "unified-extensions-button"
              ];
              TabsToolbar =
              [
                "firefox-view-button"
                "tabbrowser-tabs"
                "new-tab-button"
                "alltabs-button"
              ];
              unified-extensions-area = [
                "_12cf650b-1822-40aa-bff0-996df6948878_-browser-action"
                "jid1-bofifl9vbdl2zq_jetpack-browser-action"
                "ublock0_raymondhill_net-browser-action"
                "2_0_disconnect_me-browser-action"
                "_vivaldi-fox-browser-action"
                "cookieautodelete_kennydo_com-browser-action"
                "plasma-browser-integration_kde_org-browser-action"
              ];
              currentVersion = "20";
              newElementCount= "20";
            };
          };


        # ???
        "browser.theme.toolbar-theme" = 1;
        "network.dns.disablePrefetch" = true;
        "privacy.query_stripping.enabled" = true;
        "privacy.query_stripping.enabled.pbmode" = true;
        "extensions.privatebrowsing.notification" = true;
        "privacy.partition.network_state.ocsp_cache" = true;
        "privacy.annotate_channels.strict_list.enabled" = true;
      };
    };
  };
}
