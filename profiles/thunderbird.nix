{ pkgs, ...}:

{
  home-manager.users.romain.programs.thunderbird = {
    enable = true;
    profiles.default = {
      isDefault = true;
      settings = {
        # Traduction (insuffisant : besoin de récupérer l’extension)
        "intl.locale.requested" = "fr";
        "intl.accept_languages" = "fr";

        # Do Not Track
        "privacy.donottrackheader.enabled" = true;

        # Vérification client par défaut
        "mail.shell.checkDefaultClient" = false;

        # Toujours demander avant de compacter
        "mail.purge.ask" = false;

        # Purger le cache en quittant
        "privacy.clearOnShutdown.cache" = true;

        # Dictionnaires pour vérification orthographique
        "spellchecker.dictionary" = "en-US,fr";

        # Disposition « classique »
        "mail.folder.views.version" = 1;
        "mail.openMessageBehavior.version" = 1;
        "mail.pane_config.dynamic" = 0;

        # Densité d’affichage (1 = normal)
        "mail.uidensity" = 1;

        # Ne pas accepter les cookies
        "network.cookie.cookieBehavior" = 2;

        # Supprimer les messages marqués comme indésirables
        "mail.spam.manualMark" = true;
        "mail.spam.manualMarkMode" = 1;

        # Étiquettes
        "mailnews.tags.$label1.tag" = "Important";
        "mailnews.tags.$label2.tag" = "Boulot";
        "mailnews.tags.$label3.tag" = "Personnel";
        "mailnews.tags.$label4.tag" = "À faire";
        "mailnews.tags.$label5.tag" = "Pour plus tard";
      };
    };
  };
}
