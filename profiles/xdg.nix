{ config, lib, ... }: let
  homeDir = "/home/romain";
in
{
  home-manager.users.romain.xdg = {
    enable = true;
    userDirs = {
      enable = true;
      desktop = "${homeDir}/Bureau";
      documents = "${homeDir}/Documents";
      download = "${homeDir}/Téléchargements";
      music = "${homeDir}/Musique";
      pictures = "${homeDir}/Images";
      videos = "${homeDir}/Vidéos";
    };
  };
}
