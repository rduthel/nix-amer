{ pkgs, ... }:

{
  home-manager.users.romain = {
    home.packages = [ pkgs.fusuma ];
    xdg.configFile."fusuma/config.yml".source = ./config.yml;
    home.file = {
        ".config/autostart/fusuma.desktop" = {
          text = ''
            [Desktop Entry]
            Type=Application
            NoDisplay=true
            Terminal=false
            Name=Fusuma
            Exec=fusuma
            Icon=libinput-gestures
            Comment=Multitouch gestures with libinput driver on X11, Linux.
            Categories=System;
          '';
        };
      };
  };
}