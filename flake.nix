{
  description =
    "A collection of crap, hacks and copy-paste to make my localhosts boot";

  nixConfig.substituters = [ "https://cache.nixos.org" ];

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nix.url = "github:nixos/nix";
    flake-compat = {
      url = "github:9999years/flake-compat/fix-64";
      flake = false;
    };
    home-manager = {
        url = "github:nix-community/home-manager";
        inputs.nixpkgs.follows = "nixpkgs";
    };
    plasma-manager = {
      url = "github:pjones/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    flake-registry = {
      url = "github:nixos/flake-registry";
      flake = false;
    };

    steven-black-lists = {
        url = "github:StevenBlack/hosts";
        flake = false;
    };
  };

  outputs = { nixpkgs, self, nix, plasma-manager, ... }@inputs:
    let
      findModules = dir:
        builtins.concatLists (builtins.attrValues (builtins.mapAttrs
          (name: type:
            if type == "regular" then [{
              name = builtins.elemAt (builtins.match "(.*)\\.nix" name) 0;
              value = dir + "/${name}";
            }] else if (builtins.readDir (dir + "/${name}"))
            ? "default.nix" then [{
              inherit name;
              value = dir + "/${name}";
            }] else
              findModules (dir + "/${name}")) (builtins.readDir dir)));
      pkgsFor = system:
        import inputs.nixpkgs {
          overlays = [ self.overlay ];
          localSystem = { inherit system; };
          config = {
            android_sdk.accept_license = true;
            allowUnfreePredicate = (pkg: true);
          };
        };
    in {
      nixosModules = builtins.listToAttrs (findModules ./modules);

      nixosProfiles = builtins.listToAttrs (findModules ./profiles);

      nixosRoles = import ./roles;

      nixosConfigurations = with nixpkgs.lib;
        let
          hosts = builtins.attrNames (builtins.readDir ./machines);

          mkHost = name:
            let
              system = builtins.readFile (./machines + "/${name}/system");
              pkgs = pkgsFor system;
            in nixosSystem {
              inherit system;
              modules = __attrValues self.nixosModules ++ [
                inputs.home-manager.nixosModules.home-manager
                {
                  home-manager.sharedModules = [ plasma-manager.homeManagerModules.plasma-manager ];
                }

                (import (./machines + "/${name}"))
                { nixpkgs.pkgs = pkgs; }
                { device = name; }
              ];
              specialArgs = { inherit inputs; };
            };
        in genAttrs hosts mkHost;

      overlay = import ./overlay.nix inputs;

      lib = import ./lib.nix nixpkgs.lib;

        deploy = {
        user = "root";
        nodes = (builtins.mapAttrs (name: machine:
          let activateable = false;
          in {
            hostname = machine.config.networking.hostName;
            profiles.system = {
              user = if activateable then "root" else "romain";
            };
          }) self.nixosConfigurations);
      };
    };
}
