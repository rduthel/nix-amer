{ config, pkgs, inputs, lib, ... }:

{
  imports = [
    ./hardware-configuration.nix
    inputs.self.nixosRoles.base
    inputs.self.nixosRoles.perso
    inputs.self.nixosRoles.code
    inputs.self.nixosRoles.gaming
  ];

  deviceSpecific.devInfo = {
      cpu = {
        vendor = "amd";
        clock = 3200;
        cores = 16;
      };
      drive = {
        type = "ssd";
        speed = 6000;
        size = 250;
      };
      bigScreen = true;
      ram = 16;
    };

  nix.optimise.automatic = true;

  # https://github.com/systemd/systemd/issues/33412#issuecomment-2286210112
  systemd.units."dev-tpmrm0.device".enable = false;

  services.xserver.videoDrivers = [ "nvidia" ];

  hardware = {
    graphics = {
      enable = true;
    };
    nvidia = {
      # Modesetting is required
      modesetting.enable = true;

      powerManagement = {
        enable = false;
        finegrained = false;
      };

      nvidiaSettings = true;

      # Use the NVidia open source kernel module (not to be confused with the
      # independent third-party "nouveau" open source driver).
      # Support is limited to the Turing and later architectures. Full list of
      # supported GPUs is at:
      # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus
      # Only available from driver 515.43.04+
      # Currently "beta quality", so false is currently the recommended setting.
      open = false;

      package = config.boot.kernelPackages.nvidiaPackages.beta;
    };
  };

  networking.hostName = "the-batman";
}
