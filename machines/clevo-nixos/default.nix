{ config, pkgs, inputs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    inputs.self.nixosRoles.base
    inputs.self.nixosRoles.perso
    inputs.self.nixosRoles.code
    inputs.self.nixosRoles.gaming

    # X11 only :/
    # inputs.self.nixosProfiles.fusuma
  ];

  deviceSpecific.devInfo = {
      cpu = {
        vendor = "intel";
        clock = 1800;
        cores = 8;
      };
      drive = {
        type = "ssd";
        speed = 6000;
        size = 250;
      };
      bigScreen = true;
      ram = 16;
    };

  # officially broken
  # home-manager.users.romain.home.packages = [ pkgs.kdePackages.kamoso ];

  nix.optimise.automatic = true;
  
  # Bootloader.
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  hardware.bluetooth.enable = true;

  hardware.graphics = {
    enable = true;
  };

  services.xserver.videoDrivers = [ "nvidia" ];

  hardware.nvidia = {
    # Modesetting is required
    modesetting.enable = true;

    # Enable the Nvidia settings menu,
    # accessible via `nvidia-settings`
    nvidiaSettings = true;

    # Use the NVidia open source kernel module (not to be confused with the
    # independent third-party "nouveau" open source driver).
    # Support is limited to the Turing and later architectures. Full list of 
    # supported GPUs is at: 
    # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus 
    # Only available from driver 515.43.04+
    # Currently "beta quality", so false is currently the recommended setting.
    open = false;

    prime = {
      offload = {
        enable = true;
        enableOffloadCmd = true;
      };

      # Bus ID of the Intel GPU. You can find it using lspci, either under 3D or VGA
      intelBusId = "PCI:0:2:0";

      # Bus ID of the NVIDIA GPU. You can find it using lspci, either under 3D or VGA
      nvidiaBusId = "PCI:1:0:0";
    };
  };

  networking.hostName = "clevo-nixos";
}
