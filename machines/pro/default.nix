{ config, pkgs, ... }:

{
  imports = [
    ../../default.nix
  ];
  networking.hostName = "romain-d-nixos";

  # Enable CUPS to print documents.
  services.printing.enable = true;
}
